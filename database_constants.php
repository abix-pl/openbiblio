<?php
/*********************************************************************************
 *
 *                           A T T E N T I O N !
 *
 *  ||  Please modify the following database connection variables to match  ||
 *  \/  the MySQL database and user that you have created for OpenBiblio.   \/
 *********************************************************************************
 */
define("OBIB_HOST",     "localhost");
define("OBIB_DATABASE", "biblio_test");
define("OBIB_USERNAME", "biblio_test");
define("OBIB_PWD",      "biblio_test");
/*********************************************************************************
 *  /\                                                                      /\
 *  ||                                                                      ||
 *********************************************************************************
 */
?>
