<?php
/**
 * Author: Szymon Goralczyk <sg@apgsoft.pl>
 *
 * Lang file for inventory module. 
 */


#****************************************************************************
#*  Translation text for class Inventory
#****************************************************************************
$trans["invName"]            = "\$text = 'Inwentaryzacja';";
$trans["invDesc"]            = "\$text = 'Moduł zapewnia inwentaryzacje danych';";


// Ext for datatable :
$trans["invDataTableNr"]            = "\$text = 'Nr.';";
$trans["invDataTableAuthor"]            = "\$text = 'Autor';";
$trans["invDataTableTitle"]            = "\$text = 'Tytuł';";
$trans["invDataTableAmount"]            = "\$text = 'Ilość';";
$trans["invDataTableActions"]            = "\$text = 'Opcje';";
