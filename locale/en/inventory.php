<?php
/**
 * Author: Szymon Goralczyk <sg@apgsoft.pl>
 *
 * Lang file for inventory module.
 */


#****************************************************************************
#*  Translation text for class Biblio
#****************************************************************************

$trans["invName"]            = "\$text = 'Inventory';";
$trans["invDesc"]            = "\$text = 'Module used for inventory.';";

// Ext for datatable :
$trans["invDataTableNr"]            = "\$text = 'Nr.';";
$trans["invDataTableAuthor"]            = "\$text = 'Author';";
$trans["invDataTableTitle"]            = "\$text = 'Title';";
$trans["invDataTableAmount"]            = "\$text = 'Amount';";
$trans["invDataTableActions"]            = "\$text = 'Actions';";